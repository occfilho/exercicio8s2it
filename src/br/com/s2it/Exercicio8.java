package br.com.s2it;

public class Exercicio8 {

    private static final String A = "10256";
    private static final String B = "512";
    private static String c = "";

    public static void main(String[] args) {
        Exercicio8.criarNumeroC();
    }


    public static void criarNumeroC() {

        c = A.length() < B.length() ? concatenar(A.length()) : concatenar(B.length());

        System.out.print(c);

    }

    private static String concatenar(int length) {

        String valor = "";
        for (int i = 0; i < length; i++) {
            valor += String.valueOf(A.charAt(i)) + String.valueOf(B.charAt(i));
        }

        valor += (A.length() < B.length() ? B.substring(A.length()) : A.substring(B.length()));

        return Integer.valueOf(valor) > 1_000_000 ? "-1" : valor;

    }


}
