package br.com.s2it;

public class Exercicio9 {

    BinaryTree no1 = new BinaryTree(2);
    BinaryTree no1Left = new BinaryTree(4);
    BinaryTree no1Right = new BinaryTree(7);
    BinaryTree no12Left = new BinaryTree(3);
    BinaryTree no12Right = new BinaryTree(10);

    public static void main(String[] args) {
        Exercicio9 exercicio9 = new Exercicio9();
        exercicio9.juntarNos();
        int soma = new Exercicio9().somarNos(exercicio9.no1Left);
        System.out.println(soma);
    }


    public int somarNos(BinaryTree raiz) {

        return raiz.valor
                + (raiz.left != null ? (somarNos(raiz.left)) : 0)
                + (raiz.right != null ? (somarNos(raiz.right)) : 0);

    }


    public void juntarNos() {

        no1.left = no1Left;
        no1.right = no1Right;
        no1.left.left = no12Left;
        no1.left.right = no12Right;
        no1.right.left = no12Left;
        no1.right.right = no12Right;

    }


    public class BinaryTree {

        public BinaryTree(int valor) {
            this.valor = valor;
        }

        int valor;
        BinaryTree left;
        BinaryTree right;

    }


}
